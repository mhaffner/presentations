## load packages

## get data

## take a look at data

## take a look at a few columns

## get year as its own column

## get dui

## how many dui's per year?

## how many dui's per year? (put this in a table)

## ggplot bar plot

## how many in each census block?

## ggplot density plot

## quick web map
library(tmap)
library(sf)

kc_dui_sf <- crimedata::get_crime_data(years = 2012:2014,
                                cities = "Kansas City",
                                type = "core",
                                output = "sf") %>%
  filter(offense_type == "driving under the influence")

kc$year <- year(kc$date_single)

tmap_mode("view")
qtm(kc_dui_sf, dots.col = "year")
