---
title: "The Development of a Wisconsin Landscape Search Engine"
subtitle: "Department of Geography and Anthropology - UWEC"
author: "Matthew Haffner"
output:
  xaringan::moon_reader:
    css: ["xaringan-themer.css", "../style/xaringan/split.css"]
    lib_dir: libs
    nature:
      ratio: "16:9"
      highlightStyle: monokai
      countIncrementalSlides: false
---

```{r setup, echo = FALSE, message = FALSE, warning = FALSE, cache = TRUE}
library(leaflet)
library(sf)
library(raster)
library(dplyr)
library(dbplyr)
library(DBI)
library(stringr)
library(knitr)
library(DT)
library(rgdal)
library(RSQLite)
library(ggplot2)
library(gridExtra)
library(scales)

grid <- st_read("/home/matt/git-repos/lse-wi/data/grid.geojson", quiet = TRUE)
grid$id <- 1:nrow(grid)

## create database connection
con <- dbConnect(SQLite(), "/home/matt/git-repos/lse-wi/data/ts.db")

ts_db <- tbl(con, "ts")
lookup_db <- tbl(con, "lookup")

## create lookup dataframe now; this is fast since there are only 2500 rows
df_lookup <- lookup_db %>% collect()

dem_path <- "/home/matt/git-repos/lse-wi/data/dem/"
veg_path <- "/home/matt/git-repos/lse-wi/data/veg/"

return_sim <- function(input_id, terrain_model = "vgg16", k = 5, terrain_scale = 0.8, veg_mean_scale = 1, dist = 0, dat_tbl=ts_db, lookup_tbl = df_lookup) {
  terrain_model <- paste0(terrain_model, "_sim")

  veg_scale <- 1 - terrain_scale
  veg_sd_scale <- 1 - veg_mean_scale

  ## filter, compute similarity metric
  df_sim <- dat_tbl %>%
    dplyr::filter(parent_id == input_id) %>%
    dplyr::filter(dist > 0) %>%
    mutate(dist_mi = dist * 0.000621371) %>%
    dplyr::filter(dist_mi > !!dist) %>%
    dplyr::select(-dist) %>%
    mutate(total_sim =
             (terrain_scale * .data[[terrain_model]]) +
             (veg_scale * ((veg_mean_scale * veg_mean_sim) + (veg_sd_scale * veg_sd_sim)))) %>%
    slice_max(total_sim, n = k) %>%
    ## this executes the query and returns a data frame
    collect()

  return(df_sim)
}


## case 0
input_id <- 1165
terrain_model <- "resnet50"
k <- 5
terrain_scale <- 0.8
veg_mean_scale <- 1.0 
user_dist <- 0

df_sim_1165 <- return_sim(input_id = input_id,
                          terrain_model = terrain_model,
                          terrain_scale = terrain_scale,
                          veg_mean_scale = veg_mean_scale,
                          k = k,
                          dist = user_dist,
                          dat_tbl = ts_db,
                          lookup_tbl = df_lookup) %>%
  collect() %>%
  arrange(desc(total_sim)) %>%
  mutate(sim_rank = 1:nrow(.)) %>%
  slice(1:k) %>%
  round(3) %>%
  mutate(dist_mi = round(dist_mi, 0)) %>%
  relocate(sim_rank, dist_mi, total_sim, dist_mi, resnet50_sim, vgg16_sim, nasnet_sim, veg_mean_sim, veg_sd_sim)

## case 1
input_id <- 1521
terrain_model <- "resnet50"
k <- 5
terrain_scale <- 0.8
veg_mean_scale <- 1.0 
user_dist <- 0

df_sim_1521 <- return_sim(input_id = input_id,
                     terrain_model = terrain_model,
                     terrain_scale = terrain_scale,
                     veg_mean_scale = veg_mean_scale,
                     k = k,
                     dist = user_dist,
                     dat_tbl = ts_db,
                     lookup_tbl = df_lookup) %>%
  collect() %>%
  arrange(desc(total_sim)) %>%
  mutate(sim_rank = 1:nrow(.)) %>%
  slice(1:k) %>%
  round(3) %>%
  mutate(dist_mi = round(dist_mi, 0)) %>%
  relocate(sim_rank, dist_mi, total_sim, dist_mi, resnet50_sim, vgg16_sim, nasnet_sim, veg_mean_sim, veg_sd_sim)

## case 2
input_id <- 117
terrain_model <- "vgg16"
k <- 5
terrain_scale <- 0.5
veg_mean_scale <- 0.9
user_dist <- 0

df_sim_117 <- return_sim(input_id = input_id,
                     terrain_model = terrain_model,
                     terrain_scale = terrain_scale,
                     veg_mean_scale = veg_mean_scale,
                     k = k,
                     dist = user_dist,
                     dat_tbl = ts_db,
                     lookup_tbl = df_lookup) %>%
  collect() %>%
  arrange(desc(total_sim)) %>%
  mutate(sim_rank = 1:nrow(.)) %>%
  slice(1:k) %>%
  round(3) %>%
  mutate(dist_mi = round(dist_mi, 0)) %>%
  relocate(sim_rank, dist_mi, total_sim, dist_mi, resnet50_sim, vgg16_sim, nasnet_sim, veg_mean_sim, veg_sd_sim)

## case 3
input_id <- 2276
terrain_model <- "resnet50"
k <- 5
terrain_scale <- 0.2
veg_mean_scale <- 0.5
user_dist <- 150

df_sim_2276 <- return_sim(input_id = input_id,
                     terrain_model = terrain_model,
                     terrain_scale = terrain_scale,
                     veg_mean_scale = veg_mean_scale,
                     k = k,
                     dist = user_dist,
                     dat_tbl = ts_db,
                     lookup_tbl = df_lookup) %>%
  collect() %>%
  arrange(desc(total_sim)) %>%
  mutate(sim_rank = 1:nrow(.)) %>%
  slice(1:k) %>%
  round(3) %>%
  mutate(dist_mi = round(dist_mi, 0)) %>%
  relocate(sim_rank, dist_mi, total_sim, dist_mi, resnet50_sim, vgg16_sim, nasnet_sim, veg_mean_sim, veg_sd_sim)

plot_parent_rasters <- function(df_sim, df_lookup, dem_path, veg_path) {

  ## plot parent image dem here
  fname_dem_parent <- df_lookup %>%
    dplyr::filter(index == df_sim$parent_id %>% nth(1)) %>%
    pull(fname_dem)

  rast_dem_parent <- raster(paste0(dem_path, fname_dem_parent)) %>%
    as(., "SpatialPixelsDataFrame") %>%
    as.data.frame() %>%
    setNames(., c("Elevation", "x", "y"))

  dem_parent_plot <- ggplot(rast_dem_parent, aes(x = x, y = y, fill = Elevation)) +
    geom_raster() +
    xlab("") +
    ylab("") +
    theme_minimal() +
    theme(axis.text.x = element_blank()) +
    theme(axis.text.y = element_blank()) +
    geom_raster() +
    coord_fixed(ratio = 1) +
    labs(title = "Parent Terrain") +
    scale_fill_gradientn(colors = topo.colors(255))

  ## plot parent image veg here
  fname_veg_parent <- df_lookup %>%
    dplyr::filter(index == df_sim$parent_id %>% nth(1)) %>%
    pull(fname_veg)

  ## TODO fix units of NDVI
  rast_veg_parent <- raster(paste0(veg_path, fname_veg_parent)) %>%
    as(., "SpatialPixelsDataFrame") %>%
    as.data.frame() %>%
    setNames(., c("raw_ndvi", "x", "y")) %>%
    transmute(NDVI = round(rescale(raw_ndvi, c(-1, 1)), 2),
              x = x,
              y = y)

  veg_parent_plot <- ggplot(rast_veg_parent, aes(x = x, y = y, fill = NDVI)) +
    geom_raster() +
    xlab("") +
    ylab("") +
    theme_minimal() +
    theme(axis.text.x = element_blank()) +
    theme(axis.text.y = element_blank()) +
    labs(title = "Parent Vegetation") +
    coord_fixed(ratio = 1) +
    scale_fill_gradientn(colors = terrain.colors(255, rev = TRUE))

  grid.arrange(dem_parent_plot, veg_parent_plot, ncol = 2)
}

plot_child_rasters <- function(df_sim, df_lookup, k, dem_path, veg_path) {
  dem_plot_list <- list()
  veg_plot_list <- list()

  for (i in 1:k) {
    index_child <- df_sim %>%
      pull(child_id) %>%
      nth(i)

    fname_dem <- df_lookup %>%
      dplyr::filter(index == index_child) %>%
      dplyr::pull(fname_dem)

    rast_dem_child <- raster(paste0(dem_path, fname_dem)) %>%
      as(., "SpatialPixelsDataFrame") %>%
      as.data.frame() %>%
      setNames(., c("Elevation", "x", "y"))

    dem_plot_list[[i]] <- ggplot(rast_dem_child, aes(x = x, y = y, fill = Elevation)) +
      geom_raster() +
      xlab("") +
      ylab("") +
      theme_minimal() +
      theme(axis.text.x = element_blank()) +
      theme(axis.text.y = element_blank()) +
      theme(legend.position = "none") +
                                        #theme(plot.title = "hello") +
      theme(plot.title = element_text(size = 8)) +
      geom_raster() +
      coord_fixed(ratio = 1) +
      labs(title = paste0("Child Terrain\n", " (rank = ", i, ")")) +
      scale_fill_gradientn(colors = topo.colors(255))

    fname_veg <- df_lookup %>%
      dplyr::filter(index == index_child) %>%
      dplyr::pull(fname_veg)

    rast_veg_child <- raster(paste0(veg_path, fname_veg)) %>%
      as(., "SpatialPixelsDataFrame") %>%
      as.data.frame() %>%
      setNames(., c("NDVI", "x", "y"))

    veg_plot_list[[i]] <- ggplot(rast_veg_child, aes(x = x, y = y, fill = NDVI)) +
      geom_raster(show.legend = FALSE) +
      xlab("") +
      ylab("") +
      theme_minimal() +
      theme(axis.text.x = element_blank()) +
      theme(axis.text.y = element_blank()) +
      theme(legend.position = "none") +
      theme(plot.title = element_text(size = 8)) +
      geom_raster() +
      coord_fixed(ratio = 1) +
      labs(title = paste0("Child Vegetation\n", " (rank = ", i, ")")) +
      scale_fill_gradientn(colors = terrain.colors(255, rev = TRUE))
  }

  do.call("grid.arrange", c(dem_plot_list, veg_plot_list, nrow = 2))
}

library(RColorBrewer)

## colors
color_1 <- brewer.pal(n = 9, "Set1")[1]
color_2 <- brewer.pal(n = 9, "Set1")[2]
color_3 <- brewer.pal(n = 9, "Set1")[3]
color_4 <- brewer.pal(n = 9, "Set1")[4]
color_5 <- brewer.pal(n = 9, "Set1")[5]

fname_veg_parent <- df_lookup %>%
  dplyr::filter(index == df_sim_1165$parent_id %>% nth(1)) %>%
  pull(fname_veg)

## TODO fix units of NDVI
rast_veg_parent <- raster(paste0(veg_path, fname_veg_parent)) %>%
  as(., "SpatialPixelsDataFrame") %>%
  as.data.frame() %>%
  setNames(., c("raw_ndvi", "x", "y")) %>%
  transmute(NDVI = round(rescale(raw_ndvi, c(-1, 1)), 2),
            x = x,
            y = y)

veg_parent_plot <- ggplot(rast_veg_parent, aes(x = x, y = y, fill = NDVI)) +
  geom_raster() +
  xlab("") +
  ylab("") +
  theme_minimal() +
  theme(axis.text.x = element_blank()) +
  theme(axis.text.y = element_blank()) +
  labs(title = "NDVI") +
  coord_fixed(ratio = 1) +
  scale_fill_gradientn(colors = terrain.colors(255, rev = TRUE))

library(RefManageR)
library(knitr)

BibOptions(check.entries = TRUE,
           bib.style = "authoryear",
           cite.style = "apa",
           style = "markdown",
           hyperlink = TRUE,
           dashed = FALSE)

bib <- ReadBib(file = "/home/matt/org/work/references/references.bib")
```

```{r xaringan-themer, include = FALSE}
library(xaringanthemer)

source("../style/xaringan/xaringan.R")
spacemacs_light()
```

# Outline

## Motivation

## Measuring similarity

## Web application

## Future work

---

# Themes

--

- The relationship between (a) minimizing development time and (b) maximizing performance

- The relationship between (b) maximizing performance and (c) minimizing cloud resource usage

--

- MATH!!!!


---

# Themes

.center[
<img src='./img/math-code-meme.jpg' height='400' />
]


---

# Themes

- The relationship between (a) minimizing development time and (b) maximizing performance

- The relationship between (b) maximizing performance and (c) minimizing cloud resource usage

- ~~MATH!!!!~~

- Code!!!

---

<div align = 'center'>
    <iframe width='1366'
            src='https://uwec-geog.shinyapps.io/lse-wi'
            height='650'
            frameborder='0'
            allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen>
    </iframe>
</div>

---

# Motivation

--

.pull-left[

- Deep learning has been used in remote sensing for quite some time for tasks
  such as

  - Object detection

]

.pull-right[
.center[
<img src='https://pub.mdpi-res.com/remotesensing/remotesensing-11-00339/article_deploy/html/images/remotesensing-11-00339-g006b.png?1571063423' height='400' />

Source: `r Citet(bib, "Chen2019")`
]
]

.footnote[

]

---

# Motivation

.pull-left[

- Deep learning has been used in remote sensing for quite some time for tasks
  such as

  - Object detection
  - Object segmentation

]

.pull-right[
.center[
<img src='https://media.springernature.com/lw685/springer-static/image/art%3A10.1007%2Fs11554-020-00990-z/MediaObjects/11554_2020_990_Fig5_HTML.png' height='400' />

Source: `r Citet(bib, "Song2020")`
]
]

---

# Motivation

.pull-left[

- Deep learning has been used in remote sensing for quite some time for tasks
  such as

  - Object detection
  - Object segmentation
  - Land use classification

]

.pull-right[
.center[
<img src='https://www.mdpi.com/remotesensing/remotesensing-11-00164/article_deploy/html/images/remotesensing-11-00164-g005.png' height='400' />

Source: `r Citet(bib, "Xie2019")`
]
]

---

# Motivation

.pull-left[

- Deep learning has been used in remote sensing for quite some time for tasks
  such as

  - Object detection
  - Object segmentation
  - Land use classification

- Image retrieval (i.e. image similarity) applications have been lacking outside
  of applications which use photography

]

.pull-right[
.center[
<img src='http://slideplayer.com/slide/3415344/12/images/6/Content-based+Image+Retrieval.jpg' height='400' />

Source: [http://slideplayer.com/.../Content-based+Image+Retrieval.jpg](http://slideplayer.com/slide/3415344/12/images/6/Content-based+Image+Retrieval.jpg)
]
]

---

# Motivation

- Is it possible to apply the technique of image retrieval to landscape
  characteristics (elevation, vegetation amount, and vegetation variability)?

---

<div align = 'center'>
    <iframe width='1366'
            src='https://convnetplayground.fastforwardlabs.com/#/'
            height='650'
            frameborder='0'
            allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen>
    </iframe>
</div>

---

.pull-left[
# Motivation

.center[
<img src='img/family-01.jpg' height='350' />

_A random photograph selected from the internet that possesses significant color
variation_
]
]

.pull-right[
<br>
.center[
```{r echo=FALSE, eval=TRUE, warning=FALSE, message=FALSE, cache=TRUE}
fname_dem_parent <- df_lookup %>%
  dplyr::filter(index == df_sim_1165$parent_id %>% nth(1)) %>%
  pull(fname_dem)

rast_dem_parent <- raster(paste0(dem_path, fname_dem_parent)) %>%
  as(., "SpatialPixelsDataFrame") %>%
  as.data.frame() %>%
  setNames(., c("Elevation", "x", "y"))

dem_parent_plot <- ggplot(rast_dem_parent, aes(x = x, y = y, fill = Elevation)) +
  geom_raster() +
  xlab("") +
  ylab("") +
  theme_minimal() +
  theme(axis.text.x = element_blank()) +
  theme(axis.text.y = element_blank()) +
  geom_raster() +
  coord_fixed(ratio = 1) +
  labs(title = "") +
  scale_fill_gradientn(colors = gray.colors(255), name = "Elevation (m)")

dem_parent_plot
```

_DEM of Eau Claire, Wisconsin_
]
]

---

# Motivation

- Is it possible to apply the technique of image retrieval to landscape
  characteristics (elevation, vegetation) and thus create a landscape search
  engine?

--

- Can the landscape search engine be used as a location analysis tool?

---


.pull-left[
# Motivation

.center[
<img src='./img/bike-lowes-01.jpg' height='300' />
]
]

.pull-right[
<div align = 'center'>
    <iframe width='1366'
            src='https://strava-embeds.com/activity/7421832386#ns=136ca055-839d-47a8-894e-0afbe3356466&hostOrigin=null&hostPath=%2Fhome%2Fmatt%2Fgit-repos%2Fpresentations%2Fpublic%2Flse-wi%2F_index.html&hostTitle=The+Development+of+a+Wisconsin+Landscape+Search+Engine'
            height='600'
            frameborder='0'
            allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen>
    </iframe>
</div>
]

---

# Motivation

.pull-left[
```{r echo = FALSE, eval = TRUE, message = FALSE, warning = FALSE}
library(leaflet)

loc <- c(44.75599528879804, -91.46816445010484)

leaflet(width = '100%') %>%
  setView(lng = loc[2], lat = loc[1], zoom = 15) %>%
  addMarkers(-91.4671936536609, 44.759054208622935) %>%
  addProviderTiles(providers$CartoDB.Positron)
```
]

.pull-right[

<iframe width="560" height="315" 
src="https://www.youtube.com/embed/qMen-YauppU?rel=0&amp;start=40&mute=1&vq=hd720" title="YouTube video player"
frameborder="0" allow="accelerometer; autoplay; clipboard-write;
encrypted-media; gyroscope; picture-in-picture; web-share"
allowfullscreen></iframe> 

]

---

# Motivation

.pull-left[
```{r echo = FALSE, eval = TRUE, message = FALSE, warning = FALSE}
library(leaflet)

loc <- c(44.75599528879804, -91.46816445010484)

leaflet(width = '100%') %>%
  setView(lng = loc[2], lat = loc[1], zoom = 15) %>%
  addMarkers( -91.45952024970893, 44.75808744782233) %>%
  addProviderTiles(providers$CartoDB.Positron)
```
]

.pull-right[

<iframe
width="560"
height="315"
src="https://www.youtube.com/embed/9_caNiBHBq0?start=117&mute=1&vq=hd720"
title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
allowfullscreen>
  </iframe>
]

---

# Motivation

<!-- two column web map (start) -->

.pull-left[
```{r echo = FALSE, eval = TRUE, message = FALSE, warning = FALSE}
library(leaflet)

loc <- c(45.17981517236618, -91.34597898610718)

leaflet(width = '100%') %>%
  setView(lng = loc[2], lat = loc[1], zoom = 14) %>%
  addProviderTiles(providers$Stamen.Terrain)
```
]

.pull-right[
<div class="strava-embed-placeholder" data-embed-type="activity" data-embed-id="4439866432"></div><script src="https://strava-embeds.com/embed.js"></script>
]

---

# Motivation

- Is it possible to apply the technique of image retrieval to landscape
  characteristics (elevation, vegetation) and thus create a landscape search
  engine?

- Can the landscape search engine be used as a location analysis tool?

--

- Can a landscape search engine which uses neural network models run on a
  machine with 1 CPU core and 1 GB of RAM?!?!


---

# Data

```{r maps-dem-vegetation, echo = FALSE, eval = TRUE, message = FALSE, warning = FALSE, cache = TRUE, fig.cap = "Map of terrain (DEM) and vegetation (NDVI) data", dpi = 200, fig.height = 3}
rast_dem <- raster("https://gitlab.com/mhaffner/data/-/raw/master/wi_dem_small.tif") %>%
  raster::aggregate(., fact = 6)
rast_veg <- raster("https://gitlab.com/mhaffner/data/-/raw/master/wi_veg.tif") %>%
  raster::aggregate(., fact = 4)

rast_dem_df <- rast_dem %>% as(., "SpatialPixelsDataFrame") %>%
  as.data.frame() %>%
  setNames(., c("Elevation", "x", "y"))

## TODO set 0 to NA?
rast_veg_df <- rast_veg %>%
  as(., "SpatialPixelsDataFrame") %>%
  as.data.frame() %>%
  setNames(., c("raw_ndvi", "x", "y")) %>%
  transmute(NDVI = round(rescale(raw_ndvi, c(-1, 1)), 2) %>% na_if(., -.66),
            x = x,
            y = y)

 dem_plot <- ggplot(rast_dem_df, aes(x = x, y = y, fill = Elevation)) +
    geom_raster() +
    xlab("") +
    ylab("") +
    theme_minimal() +
    theme(axis.text.x = element_blank()) +
    theme(axis.text.y = element_blank()) +
    geom_raster() +
    coord_fixed(ratio = 1) +
    labs(title = "Terrain (DEM)") +
    scale_fill_gradientn(colors = topo.colors(255))

veg_plot <- ggplot(rast_veg_df, aes(x = x, y = y, fill = NDVI)) +
  geom_raster() +
  xlab("") +
  ylab("") +
  theme_minimal() +
  theme(axis.text.x = element_blank()) +
  theme(axis.text.y = element_blank()) +
  geom_raster() +
  coord_fixed(ratio = 1) +
  labs(title = "Vegetation (NDVI)") +
  scale_fill_gradientn(colors = terrain.colors(255, rev = TRUE), na.value = "white")

grid.arrange(dem_plot, veg_plot, ncol = 2)
```

---

# Measuring similarity

.pull-left[
## Elevation data

### VGG16

### Resnet-50

### NasNet
]

.pull-right[
.center[
<img src='./img/python-01.png' height='300' />
]
]

---

# Measuring similarity

.pull-left[
## Elevation data

```{python echo=TRUE, eval=FALSE, warning=FALSE, message=FALSE}
[0.0, 0.0, 1.0638704299926758, 0.24310043454170227, 0.0, 0.0, ... 0.0]
```
]

.pull-right[
.center[
```{r echo=FALSE, eval=TRUE, warning=FALSE, message=FALSE}
dem_parent_plot
```
<br>
_DEM of Eau Claire, Wisconsin_
]
]

---

# Measuring similarity

.pull-left[
## Vegetation data

### NDVI mean

### NDVI standard deviation
]

.pull-right[
.center[
<img src='https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/R_logo.svg/724px-R_logo.svg.png?20160212050515' height='300' />
]
]

---

# Measuring similarity

.pull-left[
## Vegetation data

- Mean: 0.009766053

- Standard deviation: 0.3311448
]

.pull-right[
```{r echo=FALSE, eval=TRUE, warning=FALSE, message=FALSE}
fname_veg_parent <- df_lookup %>%
  dplyr::filter(index == df_sim_1165$parent_id %>% nth(1)) %>%
  pull(fname_veg)

## TODO fix units of NDVI
rast_veg_parent <- raster(paste0(veg_path, fname_veg_parent)) %>%
  as(., "SpatialPixelsDataFrame") %>%
  as.data.frame() %>%
  setNames(., c("raw_ndvi", "x", "y")) %>%
  transmute(NDVI = round(rescale(raw_ndvi, c(-1, 1)), 2),
            x = x,
            y = y)

veg_parent_plot <- ggplot(rast_veg_parent, aes(x = x, y = y, fill = NDVI)) +
  geom_raster() +
  xlab("") +
  ylab("") +
  theme_minimal() +
  theme(axis.text.x = element_blank()) +
  theme(axis.text.y = element_blank()) +
  labs(title = "") +
  coord_fixed(ratio = 1) +
  scale_fill_gradientn(colors = terrain.colors(255, rev = TRUE))

veg_parent_plot
```
]

---

# Measuring similarity

$$\mbox{total_sim} = (\mbox{terrain_scale} * \mbox{model_sim}) +$$
$$(\mbox{veg_scale} * ((\mbox{veg_mean_scale} * \mbox{veg_mean_sim}) +
(\mbox{veg_sd_scale} * \mbox{veg_sd_sim}))$$

Where

$$\mbox{veg_scale} = 1 - \mbox{terrain_scale}$$


---

# Web application

.center[
<img src='./img/sqlite-01.png' height='400' />
]

---

# Web application

```{r echo=FALSE, eval=TRUE, warning=FALSE, message=FALSE}
con <- dbConnect(SQLite(), "/home/matt/git-repos/lse-wi/data/ts.db")

ts_db <- tbl(con, "ts")
ts_db %>% print()
```

---

# Web application

## Data size: 450 MB

<div align="center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/KPGmoElPn1s?start=8&mute=1" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>

---

# Web application
.pull-left[
.center[
<img src='./img/shiny-01.png' height='300' />
]
]

.pull-right[
.center[
<img src='./img/dbplyr-01.jpg' height='400' />
]
]

---

# Web application

.center[
<img src='./img/dplyr-01.gif' height='400' />
]

---

# Web application

## Retrieving results

```{r echo=TRUE, eval=FALSE}
db
```

---

# Web application

## Retrieving results

```{r echo=TRUE, eval=FALSE}
db |>
  filter(parent_id == input_id)
```

---

# Web application

## Retrieving results

```{r echo=TRUE, eval=FALSE}
db |>
  filter(parent_id == input_id) |>
```
---

# Web application

## Retrieving results

```{r echo=TRUE, eval=FALSE}
db |>
  filter(parent_id == input_id) |>
  mutate(dist_mi = dist * 0.000621371)
```

---

# Web application

## Retrieving results

```{r echo=TRUE, eval=FALSE}
db |>
  filter(parent_id == input_id) |>
  mutate(dist_mi = dist * 0.000621371) |>
  filter(dist_mi > exclusion_dist)
```
---

# Web application

## Retrieving results

```{r echo=TRUE, eval=FALSE}
db |>
  filter(parent_id == input_id) |>
  mutate(dist_mi = dist * 0.000621371) |>
  filter(dist_mi > exclusion_dist) |>
  select(-dist) |>
```
---

# Web application

## Retrieving results

```{r echo=TRUE, eval=FALSE}
db |>
  filter(parent_id == input_id) |>
  mutate(dist_mi = dist * 0.000621371) |>
  filter(dist_mi > exclusion_dist) |>
  select(-dist) |>
  mutate(total_sim =
           (terrain_scale * .data[[terrain_model]]) +
           (veg_scale * ((veg_mean_scale * veg_mean_sim) + (veg_sd_scale * veg_sd_sim))))
```

---

# Web application

## Retrieving results

```{r echo=TRUE, eval=FALSE}
db |>
  filter(parent_id == input_id) |>
  mutate(dist_mi = dist * 0.000621371) |>
  filter(dist_mi > exclusion_dist) |>
  select(-dist) |>
  mutate(total_sim =
           (terrain_scale * .data[[terrain_model]]) +
           (veg_scale * ((veg_mean_scale * veg_mean_sim) + (veg_sd_scale * veg_sd_sim)))) |>
  collect()
```
---

# Web application

## Retrieving results

```{r echo=TRUE, eval=FALSE}
db |>
  filter(parent_id == input_id) |>
  mutate(dist_mi = dist * 0.000621371) |>
  filter(dist_mi > exclusion_dist) |>
  select(-dist) |>
  mutate(total_sim =
           (terrain_scale * .data[[terrain_model]]) +
           (veg_scale * ((veg_mean_scale * veg_mean_sim) + (veg_sd_scale * veg_sd_sim)))) |>
  collect() |>
  arrange(desc(total_sim))
```
---

# Web application

## Retrieving results

```{r echo=TRUE, eval=FALSE}
db |>
  filter(parent_id == input_id) |>
  mutate(dist_mi = dist * 0.000621371) |>
  filter(dist_mi > exclusion_dist) |>
  select(-dist) |>
  mutate(total_sim =
           (terrain_scale * .data[[terrain_model]]) +
           (veg_scale * ((veg_mean_scale * veg_mean_sim) + (veg_sd_scale * veg_sd_sim)))) |>
  collect() |>
  arrange(desc(total_sim)) |>
  mutate(sim_rank = 1:nrow(.))
```
---

# Web application

## Retrieving results

```{r echo=TRUE, eval=FALSE}
db |>
  filter(parent_id == input_id) |>
  mutate(dist_mi = dist * 0.000621371) |>
  filter(dist_mi > exclusion_dist) |>
  select(-dist) |>
  mutate(total_sim =
           (terrain_scale * .data[[terrain_model]]) +
           (veg_scale * ((veg_mean_scale * veg_mean_sim) + (veg_sd_scale * veg_sd_sim)))) |>
  collect() |>
  arrange(desc(total_sim)) |>
  mutate(sim_rank = 1:nrow(.)) |>
  slice(1:k)
```

---

# Web application

## Retrieving results

```{r echo=TRUE, eval=FALSE}
db |>
  filter(parent_id == input_id) |>
  mutate(dist_mi = dist * 0.000621371) |>
  filter(dist_mi > exclusion_dist) |>
  select(-dist) |>
  mutate(total_sim =
           (terrain_scale * .data[[terrain_model]]) +
           (veg_scale * ((veg_mean_scale * veg_mean_sim) + (veg_sd_scale * veg_sd_sim)))) |>
  collect() |>
  arrange(desc(total_sim)) |>
  mutate(sim_rank = 1:nrow(.)) |>
  slice(1:k) |>
  round(3)
```

---

# Web application

## Retrieving results

```{sql echo=TRUE, eval=FALSE, warning=FALSE, message=FALSE}
SELECT *
FROM
  (SELECT parent_id, child_id, resnet50_sim, vgg16_sim, nasnet_sim, veg_mean_sim, veg_sd_sim, dist_mi, total_sim, RANK() OVER (ORDER BY total_sim DESC) AS q01
FROM
  (SELECT parent_id, child_id, resnet50_sim, vgg16_sim, nasnet_sim, veg_mean_sim, veg_sd_sim, dist_mi, (0.8 * vgg16_sim) + (0.2 * ((1.0 * veg_mean_sim) + (0.0 * veg_sd_sim))) AS total_sim
FROM
  (SELECT parent_id, child_id, resnet50_sim, vgg16_sim, nasnet_sim, veg_mean_sim, veg_sd_sim, dist, dist * 0.000621371 AS dist_mi
FROM
  (SELECT *
FROM ts
WHERE (parent_id = 2276.0))
WHERE (dist > 0.0))
WHERE (dist_mi > 0.0)))
WHERE (q01 <= 5)
```

---

# Web application

## Results

.pull-left-70[
<!-- one column web map (start) -->

```{r echo = FALSE, eval = TRUE, message = FALSE, warning = FALSE}
library(leaflet)

loc <- grid %>%
  filter(id == 1521) %>%
  st_centroid() %>%
  mutate(x = st_coordinates(.) %>% data.frame() %>% pull(X),
         y = st_coordinates(.) %>% data.frame() %>% pull(Y))

leaflet(width = '100%', height = "400px") %>%
  addProviderTiles(providers$Stamen.Terrain) %>%
  addMarkers(lng = loc$x, lat = loc$y ) %>%
  setView(lat= 44.59851309289483, lng = -89.63812614721589, zoom = 6)
```

<!-- one column web map (end) -->

]

.pull-right-30[
- `k` = 5
- `terrain_model` = 'resnet50'
- `terrain_scale` = 0.8
- `veg_mean_scale` = 1.0
- `user_dist` = 0
]

---

.center[
```{r parent-rasters-1521, echo = FALSE, eval = TRUE, message = FALSE, warning = FALSE, cache = TRUE, fig.height = 4, out.extra = ""}
plot_parent_rasters(df_sim_1521,
                    df_lookup,
                    dem_path = "/home/matt/git-repos/landscape-search-engine/data-spatial/dem-ind-non-empty-tif/",
                    veg_path = "/home/matt/git-repos/landscape-search-engine/data-spatial/modis-ind-tif/")
```

```{r child-rasters-1521, echo = FALSE, eval = TRUE, message = FALSE, warning = FALSE, cache = TRUE, fig.height = 3, out.extra = ""}
plot_child_rasters(df_sim_1521,
                   df_lookup,
                   k=5,
                   dem_path = "/home/matt/git-repos/landscape-search-engine/data-spatial/dem-ind-non-empty-tif/",
                   veg_path = "/home/matt/git-repos/landscape-search-engine/data-spatial/modis-ind-tif/")
```
]

---

# Web application

## Live demo

- https://uwec-geog.shinyapps.io/lse-wi

---

# Current limitations

- Resolution is too low

- Grid cells ought to be smaller

--

# Future directions

- A multi-input neural network model specifically for landscape search.....but
  this will require a lot of training data

- An option for user input of DEM/vegetation data

---

# Acknowledgments

- This research was funded by the Office of Research and Sponsored Programs at
  the University of Wisconsin - Eau Claire.

- I'd like to thank my collaborators Matt DeWitte, Papia Rozario, and Gustavo
  Ovando-Montejo for their work on this project.

# Other info

- Slides: https://geohaff-pres.netlify.app/lse-wi/

- My website: https://geohaff.com/

- Email: haffnerm@uwec.edu

---

# References

```{r echo=FALSE, eval=TRUE, warning=FALSE, message=FALSE, results="asis"}
PrintBibliography(bib)
```
