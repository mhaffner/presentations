library(leaflet)
library(leaflet.extras)
library(sf)
library(dplyr)
library(raster)

data <- "/home/matt/git-repos/mapsurvey/example-data/prm-test"

## specify type of file and data path to create list of file directories
file.list <- list.files(pattern = "*.geojson", path = data, full.names = TRUE)

## loop for reading and binding shp files
for (i in 1:length(file.list)) {
  if (i == 1) {
    poly.sf <- sf::st_read(file.list[i], quiet = TRUE)
  } else {
    tmp.sf <- sf::st_read(file.list[i], quiet = TRUE)
    poly.sf <- rbind(poly.sf, tmp.sf)
  }
}

## this can all by simplified a bit (you can remove lines 23 - 46)
poly.sp <- poly.sf %>%
  lwgeom::st_make_valid() %>%
  dplyr::distinct() %>%
  filter(
    st_geometry_type(.) %in%
    c("POLYGON", "MULTIPOLYGON")) %>%
  as("Spatial")

rast <- polygonsToRaster(poly.sp)
