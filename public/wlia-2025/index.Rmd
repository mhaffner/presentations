---
title: "From Theory to Practice: Cultivating Geospatial Success in Industry"
subtitle: "WLIA 2025"
author:
  - "Matthew Haffner, PhD"
  - "https://geohaff-pres.netlify.app/wlia-2025/"
output:
  xaringan::moon_reader:
    css: ["xaringan-themer.css", "../style/xaringan/split.css"]
    lib_dir: libs
    nature:
      ratio: "16:9"
      highlightStyle: monokai
      countIncrementalSlides: false
---
class:inverse

```{r xaringan-themer, include = FALSE}
library(xaringanthemer)

source("../style/xaringan/xaringan.R")
spacemacs_light_bigger_text()

## TODO import theme specific css files here (for more fine grained control like text shadow)
```

# Outline

- My background, perspective on academic GIS, and perceptions on industry

- Current developments in the academy

- Advice for job seekers

---

.pull-left-40[
# Perspective on the GIS industry

## My background

- B.S.Ed. in Mathematics and Geography
- M.S. in Geography
- Ph.D. in Geography
]

.pull-right-60[
.center[
<div style='max-height:625px'>
    <img style='max-height: inherit'
         src='https://upload.wikimedia.org/wikipedia/en/thumb/0/0e/Pittsburg_State_University_seal.svg/1200px-Pittsburg_State_University_seal.svg.png'
         height='300px' />
</div>
.small[]
]

.center[
<div style='max-height:625px'>
    <img style='max-height: inherit'
         src='https://upload.wikimedia.org/wikipedia/en/thumb/f/f4/Oklahoma_State_University_seal.svg/1200px-Oklahoma_State_University_seal.svg.png'
         height='300px' />
</div>
.small[]
]

]

---

.pull-left-40[
# Perspective on the GIS industry

## My background

- Associate Professor at UWEC
- Research Affiliate with the Mayo Clinic
- Independent Consultant
]

.pull-right-60[
.center[
<div style='max-height:625px'>
    <img style='max-height: inherit'
         src='https://upload.wikimedia.org/wikipedia/en/3/34/UW%E2%80%93Eau_Claire_seal.png'
         height='300px' />
</div>
.small[]
]

.center[
<div style='max-height:625px'>
    <img style='max-height: inherit'
         src='https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Mayo_Clinic_logo.svg/512px-Mayo_Clinic_logo.svg.png'
         height='300px' />
</div>
.small[]
]

]

---


.pull-left-40[
# Perspective on the GIS industry
.center[
<div style='max-height:625px'>
    <img style='max-height: inherit'
         src='https://upload.wikimedia.org/wikipedia/en/3/34/UW%E2%80%93Eau_Claire_seal.png'
         height='100%' />
</div>
.small[]
]
]

.pull-right-60[
## My courses
.small[
- Geospatial
  - GEOG 335 Geographic Information Systems I
  - GEOG 337 Geographic Information Systems II
  - GEOG 435 Geographic Information Systems III
  - GEOG 370 Quantitative Methods in Geography

- Human/urban
  - GEOG 111 Planet Earth: Human Geography
  - GEOG 270 Introduction to Urban and Regional Planning
  - GEOG 470 Urban Geography

- Others
  - GEOG 401 Capstone Seminar
  - GEOG 498 Geography Internship
  - GEOG 368 Geography Field Seminar
]
]

---

.pull-left-40[
# Perspective on the GIS industry
.center[
<div style='max-height:625px'>
    <img style='max-height: inherit'
         src='https://upload.wikimedia.org/wikipedia/en/3/34/UW%E2%80%93Eau_Claire_seal.png'
         height='100%' />
</div>
.small[]
]
]

.pull-right-60[
## Department geospatial courses
.small[
- GEOG 135 Planet Earth: Our Digital Globe
- GEOG 280 Introduction to Cartography and Visualization
- GEOG 339 Applied Cartography and Geovisualization 
- GEOG 336 Geospatial Field Methods
- GEOG 335 Geographic Information Systems I
- GEOG 337 Geographic Information Systems II
- GEOG 435 Geographic Information Systems III
- GEOG 370 Quantitative Methods in Geography
- GEOG 338 Remote Sensing of the Environment
- GEOG 438 Remote Sensing Data Analytics
- GEOG 352 Business Geographics 
- GEOG 355 Biogeography
- GEOG 358 LiDAR Analysis & Applications 
- GEOG 363 Watershed Analysis
- GEOG 390 Geospatial Applications of UAS
- GEOG 455 Web Geographic Information Systems
]
]

---
class:inverse,center,transition

## Shortcomings of academic GIS

---


.pull-left[
# Perspective on the GIS industry

## Shortcomings of academic GIS

- Disconnect between research tasks and industry tasks


]

.pull-right[
.center[
<div style='max-height:625px'>
    <img style='max-height: inherit'
         src='../img/gis-debate-02.jpg'
         height='100%' />
</div>
.small[]
]

]

---


.pull-left[
# Perspective on the GIS industry

## Shortcomings of academic GIS

- Disconnect between research tasks and industry tasks

- Not enough focus on business, marketing, and entrepreneurship

]

.pull-right[
.center[
<div style='max-height:625px'>
    <img style='max-height: inherit'
         src='../img/business-meeting-01.jpg'
         height='100%' />
</div>
.small[]
]

]

---

.pull-left[
# Perspective on the GIS industry

## Shortcomings of academic GIS

- Coursework is not technical enough!
]

.pull-right[
.center[
<div style='max-height:625px'>
    <img style='max-height: inherit'
         src='https://miro.medium.com/v2/resize:fit:1400/0*0F5WwhadUwSYMmzz.png'
         height='100%' />
</div>
.small[]
]
.center[
<div style='max-height:625px'>
<img style='max-height: inherit' src='https://brandlogos.net/wp-content/uploads/2021/11/git-logo.png' height='200px' />
</div>
.small[]
]

.center[
<div style='max-height:625px'>
<img style='max-height: inherit' src='https://upload.wikimedia.org/wikipedia/commons/thumb/5/5f/Emacs-logo.svg/2548px-Emacs-logo.svg.png' height='200px' />
</div>
.small[]
]
]

---
class:inverse,center,transition

## Pinchpoints between the academy and industry

---

.pull-left[
# Perspective on the GIS industry

## Pinchpoints between the academy and industry

- Concepts vs. software
]

.pull-right[
.center[
<div style='max-height:625px'>
<img style='max-height: inherit' src='../img/gis-debate-01.jpg' height='100%' />
</div>
.small[]
]
]

---
class:inverse

# Perspective on the GIS industry

## Comment from an alum

>  ...I had to explain to the whole staff (Including GIS) there what an EPSG code was... I was rather surprised...

<div></div>

--

> ...It was kind of interesting comparing my knowledge of GIS to another recent hire at the firm. He had way more knowledge of ESRI, specifically ArcGIS Online. I knew more of the geospatial functions and the programming though. They had never used the "Summarize" function, and were always exporting to Excel to use a "Pivot Table," whatever that is. Kinda blew their minds that they could do it in 2 seconds lol...

---

.pull-left[
# Perspective on the GIS industry

## Pinchpoints between the academy and industry

- Concepts vs. software

- Software discrepancies
]

.pull-right[
.center[
<div style='max-height:625px'>
<img style='max-height: inherit' src='https://novorender.com/wp-content/uploads/2024/03/ArcGIS_logo-1.png' height='100%' />
</div>
.small[]
]
]

---

.pull-left[
# Perspective on the GIS industry

## Pinchpoints between the academy and industry

- Concepts vs. software

- Software discrepancies
]

.pull-right[
.center[
<div style='max-height:625px'>
<img style='max-height: inherit' src='https://www.revitec.tech/wp-content/uploads/2023/12/AutoCAD.jpg' height='100%' />
</div>
.small[]
]
]

---

.pull-left[
# Perspective on the GIS industry

## Pinchpoints between the academy and industry

- Concepts vs. software

- Software discrepancies
]

.pull-right[
.center[
<div style='max-height:625px'>
<img style='max-height: inherit' src='https://qgis.org/img/logosign.svg' height='100%' />
</div>
.small[]
]
]

---

.pull-left[
# Perspective on the GIS industry

## Pinchpoints between the academy and industry

- Concepts vs. software

- Software discrepancies
]

.pull-right[
.center[
<div style='max-height:625px'>
<img style='max-height: inherit' src='https://informedinfrastructure.com/wp-content/uploads/2024/05/TBC-e1715605280226.png' height='100%' />
</div>
.small[]
<div style='max-height:625px'>
<img style='max-height: inherit' src='https://cdn11.bigcommerce.com/s-qj42c2ep/images/stencil/1280x1280/products/689/1606/s5_1__63665.1449264799.jpg?c=2' height='300px' />
</div>
]
]

---


.pull-left[
# Perspective on the GIS industry

## Pinchpoints between the academy and industry

- Concepts vs. software

- Software discrepancies
]

.pull-right[
.center[
<div style='max-height:625px'>
<img style='max-height: inherit' src='https://upload.wikimedia.org/wikipedia/commons/8/87/Sql_data_base_with_logo.png' height='100%' />
<img style='max-height: inherit' src='https://www.fullstackpython.com/img/logos/py.png' height='100%' />
<img style='max-height: inherit' src='https://logos-world.net/wp-content/uploads/2021/08/Amazon-Web-Services-AWS-Logo.png' height='200px' />
</div>
.small[]
]
]

---
class:inverse,center,transition

## What's going on in the academy?

---

.pull-left[
# What's going on in the academy?

- Enrollment decline

]

.pull-right[
.center[
<div style='max-height:625px'>
    <img style='max-height: inherit'
         src='https://econofact.org/wp-content/uploads/2022/06/1.1-Gawe_Desktop-2.png'
         height='100%' />
</div>
.small[]
]
]

---

.pull-left[
# What's going on in the academy?

- Enrollment decline

- The "demographic cliff"

]

.pull-right[
.center[
<div style='max-height:625px'>
    <img style='max-height: inherit'
         src='https://econsultsolutions.com/wp-content/uploads/2024/01/Higher-Ed-Cliff-Inclusion.jpg'
         height='100%' />
</div>
.small[]
]
]

---


.pull-left[
# What's going on in the academy?

- Enrollment decline

- The "demographic cliff"

- Hiring freezes
]

.pull-right[
.center[
<div style='max-height:625px'>
    <img style='max-height: inherit'
         src='../img/hiring-freeze-01.jpg'
         height='100%' />
</div>
.small[]
]
]

---


.pull-left[
# What's going on in the academy?

- Enrollment decline

- The "demographic cliff"

- Hiring freezes

- Budget issues
]

.pull-right[
.center[
<div style='max-height:625px'>
    <img style='max-height: inherit'
         src='../img/budget-issues-01.jpg'
         height='100%' />
</div>
.small[]
]
]

---

.pull-left[
# What's going on in the academy?

- Enrollment decline

- The "demographic cliff"

- Hiring freezes

- Budget issues

- Challenges associated with AI
]

.pull-right[
.center[
<div style='max-height:625px'>
    <img style='max-height: inherit'
         src='../img/ai-01.jpg'
         height='100%' />
</div>
.small[]
]
]

---
class:inverse,center,transition

## Advice for job seekers

---

.pull-left[
## Questions I receive from employers (as a reference)

- Does the person show up and show up on time?

]

.pull-right[
.center[
<div style='max-height:625px'>
    <img style='max-height: inherit'
         src='../img/running-late-01.jpg'
         height='100%' />
</div>
.small[]
]
]

---

.pull-left[
## Questions I receive from employers (as a reference)

- Does the person show up and show up on time?

- What's the quality of their work?
]

.pull-right[
.center[
<div style='max-height:625px'>
    <img style='max-height: inherit'
         src='../img/high-quality-gis-work-01.jpg'
         height='100%' />
</div>
.small[]
]
]

---

.pull-left[
## Questions I receive from employers (as a reference)

- Does the person show up and show up on time?

- What's the quality of their work?

- What are their strengths and weaknesses?

]

.pull-right[
.center[
<div style='max-height:625px'>
    <img style='max-height: inherit'
         src='../img/cabin-01.jpg'
         height='100%' />
</div>
.small[]
]
]

---

.pull-left[
## Questions I receive from employers (as a reference)

- Does the person show up and show up on time?

- What's the quality of their work?

- What are their strengths and weaknesses?

- Would you hire this person?
]

.pull-right[
.center[
<div style='max-height:625px'>
    <img style='max-height: inherit'
         src='../img/cabin-02.jpg'
         height='100%' />
</div>
.small[]
]
]

---

.pull-left[
## My suggestions

- Tackle projects!

]

.pull-right[
.center[
<div style='max-height:625px'>
    <img style='max-height: inherit'
         src='https://kottke.org/cdn-cgi/image/format=auto,fit=scale-down,width=1200,metadata=none/plus/misc/images/scott-reinhard-01.jpg'
         height='100%' />
</div>
.small[]
]
]

---

.pull-left[
## My suggestions

- Tackle projects!

- Learn principles (and don't be a "software collector")

]

.pull-right[
.center[
<div style='max-height:625px'>
    <img style='max-height: inherit'
         src='https://www.schoolofit.co.za/wp-content/uploads/2024/08/20-best-programming-languages-in-2024-ultimate-list.jpg'
         height='100%' />
</div>
.small[]
]
]

---


.pull-left[
## My suggestions

- Tackle projects!

- Learn principles (and don't be a "software collector")

- Build an online presence

]

.pull-right[
.center[
<div style='max-height:625px'>
    <img style='max-height: inherit'
         src='../img/linkedin-01.webp'
         height='100%' />
</div>
.small[]
]
]

---


.pull-left[
## My suggestions

- Tackle projects!

- Learn principles (and don't be a "software collector")

- Build an online presence

- Network and be a team player
]

.pull-right[
.center[
<div style='max-height:625px'>
    <img style='max-height: inherit'
         src='https://assets.noviams.com/novi-file-uploads/wlia/images/2025_Annual_Conference/WLIAAC2025LogoDraft20241008StandardLogo.png'
         height='100%' />
</div>
.small[]
]
]

---

.pull-left[
## My suggestions

- Tackle projects!

- Learn principles (and don't be a "software collector")

- Build an online presence

- Network and be a team player

- Build your technical skills
]

.pull-right[
.center[
<div style='max-height:625px'>
    <img style='max-height: inherit'
         src='https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/R_logo.svg/1280px-R_logo.svg.png'
         height='100%' />
</div>
.small[]
]
]

---

.pull-left[
## My suggestions

- Tackle projects!

- Learn principles (and don't be a "software collector")

- Build an online presence

- Network and be a team player

- Build your technical skills

- Interview preparation

]

.pull-right[
.center[
<div style='max-height:625px'>
    <img style='max-height: inherit'
         src=''
         height='100%' />
</div>
.small[]
]
]

---

<div align = 'center'>
    <iframe width='1366'
            src='https://geohaff.com/post/entry-level-gis-interview-prepartion/'
            height='625'
            frameborder='0'
            allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen>
    </iframe>
</div>

---

.pull-left[
## My suggestions

- Tackle projects!

- Learn principles (and don't be a "software collector")

- Build an online presence

- Network and be a team player

- Build your technical skills

- Interview preparation

- _Apply for positions_
]

.pull-right[
.center[
<div style='max-height:625px'>
    <img style='max-height: inherit'
         src='https://www.thebillfold.com/wp-content/uploads/2015/02/0Dp9Upxm7QBAih3g2.jpg'
         height='100%' />
</div>
.small[]
]
]

---
class:inverse

# Thank you

- This presentation: https://geohaff-pres.netlify.app/wlia-2025/

- My website: https://geohaff.com

- Interview questions: https://geohaff.com/post/entry-level-gis-interview-prepartion/

- My email: haffnerm@uwec.edu

